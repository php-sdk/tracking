<?php

namespace Econda\Tracking;
use PHPUnit\Framework\TestCase;

class PageViewTest extends TestCase {

    public function testDataLayerMustContainLangId() {
        $pv = new PageView();
        $pv->langId = 'en';
        $dataLayer = $pv->getDataLayer();
        $this->assertEquals('en', $dataLayer['langid']);
    }

    public function testDataLayerMustContainCountryId() {
        $pv = new PageView();
        $pv->countryId = "DE";
        $dataLayer = $pv->getDataLayer();
        $this->assertEquals('DE', $dataLayer['countryid']);
    }

    public function testDataLayerMustContainSiteId() {
        $pv = new PageView();
        $pv->siteId = "mydomain.com";
        $dataLayer = $pv->getDataLayer();
        $this->assertEquals('mydomain.com', $dataLayer['siteid']);
    }

    public function testDataLayerMustContainContentLabel() {
        $pv = new PageView();
        $pv->contentLabel = 'My/Page';
        $dataLayer = $pv->getDataLayer();
        $this->assertEquals('My/Page', $dataLayer['content']);
    }

}
