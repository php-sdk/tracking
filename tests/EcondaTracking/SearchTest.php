<?php

namespace Econda\Tracking;
use PHPUnit\Framework\TestCase;

class SearchTest extends TestCase {

    public function testWithNameInConstructor() {
        $pv = new PageView();
        $pv->add(new Search('my query', 23));
        $dataLayer = $pv->getDataLayer();
        $this->assertEquals('my query', $dataLayer['search'][0]);
        $this->assertEquals(23, $dataLayer['search'][1]);
    }

    public function testWithArrayInConstructor() {
        $pv = new PageView();
        $pv->add(new Search(['query' => 'my query', 'numberOfHits' => 23]));
        $dataLayer = $pv->getDataLayer();
        $this->assertEquals('my query', $dataLayer['search'][0]);
        $this->assertEquals(23, $dataLayer['search'][1]);
    }

    public function testWithNameSetAsProperty() {
        $search = new Search();
        $search->query = 'my query';
        $search->numberOfHits = 23;
        
        $pv = new PageView();
        $pv->add($search);
        $dataLayer = $pv->getDataLayer();
        
        $this->assertEquals('my query', $dataLayer['search'][0]);
        $this->assertEquals(23, $dataLayer['search'][1]);
    }

}
