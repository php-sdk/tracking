<?php

namespace Econda\Tracking;
use PHPUnit\Framework\TestCase;

class ProductAddToCartTest extends TestCase {

    private function getProduct() {
        $prod = new TransactionProduct(array(
            'pid' => 'PID',
            'sku' => 'SKU',
            'name' => 'PRODUCT-NAME',
            'group' => 'GROUP',
            'price' => 19.99,
        ));
        return $prod;
    }
    
    public function testWithDataInConstructor() {
        $pv = new PageView();
        $pv->add(new ProductAddToCart($this->getProduct()));
        $dataLayer = $pv->getDataLayer();
        $this->assertEquals('c_add', $dataLayer['ec_Event'][0]['type']);
        $this->assertEquals(1, $dataLayer['ec_Event'][0]['count']);
        $this->assertEquals('PID', $dataLayer['ec_Event'][0]['pid']);
        $this->assertEquals('SKU', $dataLayer['ec_Event'][0]['sku']);
        $this->assertEquals('PRODUCT-NAME', $dataLayer['ec_Event'][0]['name']);
        $this->assertEquals('GROUP', $dataLayer['ec_Event'][0]['group']);
    }
}
