<?php

namespace Econda\Tracking;
use PHPUnit\Framework\TestCase;

class LoginTest extends TestCase {

    public function testWithStringUserIdInConstructor() {
        $pv = new PageView();
        $pv->add(new Login('USER-ID', 15));
        $dataLayer = $pv->getDataLayer();
        $this->assertEquals('USER-ID', $dataLayer['login'][0]);
        $this->assertEquals(15, $dataLayer['login'][1]);
    }

    public function testWithNumericUserIdInConstructor() {
        $pv = new PageView();
        $pv->add(new Login(0, 15));
        $dataLayer = $pv->getDataLayer();
        $this->assertEquals('0', $dataLayer['login'][0]);
        $this->assertEquals(15, $dataLayer['login'][1]);
    }

    public function testWithArrayInConstructor() {
        $pv = new PageView();
        $pv->add(new Login(['userId' => 'USER-ID']));
        $dataLayer = $pv->getDataLayer();
        $this->assertEquals('USER-ID', $dataLayer['login'][0]);
    }

    public function testWithDataSetAsProperty() {
        $login = new Login();
        $login->userId = 'USER-ID';
        $login->errorCode = 15;
        
        $pv = new PageView();
        $pv->add($login);
        $dataLayer = $pv->getDataLayer();
        
        $this->assertEquals('USER-ID', $dataLayer['login'][0]);
        $this->assertEquals(15, $dataLayer['login'][1]);

    }

}
