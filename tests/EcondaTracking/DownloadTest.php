<?php

namespace Econda\Tracking;
use PHPUnit\Framework\TestCase;

class DownloadTest extends TestCase {

    public function testWithNameInConstructor() {
        $pv = new PageView();
        $pv->add(new Download('MyTestDownload.pdf'));
        $dataLayer = $pv->getDataLayer();
        $this->assertEquals('MyTestDownload.pdf', $dataLayer['download']);
    }

    public function testWithArrayInConstructor() {
        $pv = new PageView();
        $pv->add(new Download(['name' => 'MyTestDownload.pdf']));
        $dataLayer = $pv->getDataLayer();
        $this->assertEquals('MyTestDownload.pdf', $dataLayer['download']);
    }

    public function testWithNameSetAsProperty() {
        $download = new Download();
        $download->name = 'MyTestDownload.pdf';
        
        $pv = new PageView();
        $pv->add($download);
        $dataLayer = $pv->getDataLayer();
        
        $this->assertEquals('MyTestDownload.pdf', $dataLayer['download']);
    }

}
