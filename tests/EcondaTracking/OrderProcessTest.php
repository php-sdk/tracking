<?php

namespace Econda\Tracking;
use PHPUnit\Framework\TestCase;

class OrderProcessTest extends TestCase {

    public function testWithNameInConstructor() {
        $pv = new PageView();
        $pv->add(new OrderProcess('1_TEST'));
        $dataLayer = $pv->getDataLayer();
        $this->assertEquals('1_TEST', $dataLayer['orderProcess']);
    }

    public function testWithArrayInConstructor() {
        $pv = new PageView();
        $pv->add(new OrderProcess(['name' => '2_TEST']));
        $dataLayer = $pv->getDataLayer();
        $this->assertEquals('2_TEST', $dataLayer['orderProcess']);
    }

}
