<?php

namespace Econda\Tracking;
use PHPUnit\Framework\TestCase;

class OrderTest extends TestCase {

    private function getProduct() {
        $prod = new TransactionProduct(array(
            'pid' => 'PID',
            'sku' => 'SKU',
            'name' => 'PRODUCT-NAME',
            'group' => 'GROUP',
            'price' => 19.99,
            'count' => 2
        ));
        return $prod;
    }
    
    public function testWithDataInConstructor() {
        $pv = new PageView();
        $pv->add(new Order(array(
            'number' => 'MYORDERNUMBER',
            'customerId' => 'CUSTOMERID',
            'location' => 'LOCATION',
            'value' => 123.45,
            'products' => array($this->getProduct(), $this->getProduct()),
        )));
        $dataLayer = $pv->getDataLayer();
        
        $this->assertEquals('MYORDERNUMBER', $dataLayer['billing'][0]);
        $this->assertEquals('CUSTOMERID', $dataLayer['billing'][1]);
        $this->assertEquals('LOCATION', $dataLayer['billing'][2]);
        $this->assertEquals(123.45, $dataLayer['billing'][3]);
        
        $this->assertEquals('buy', $dataLayer['ec_Event'][1]['type']);
        $this->assertEquals(2, $dataLayer['ec_Event'][1]['count']);
        $this->assertEquals('PID', $dataLayer['ec_Event'][1]['pid']);
        $this->assertEquals('SKU', $dataLayer['ec_Event'][1]['sku']);
        $this->assertEquals('PRODUCT-NAME', $dataLayer['ec_Event'][1]['name']);
        $this->assertEquals('GROUP', $dataLayer['ec_Event'][1]['group']);
    }
    
}
