<?php

namespace Econda\Tracking;
use PHPUnit\Framework\TestCase;

class MarkerTest extends TestCase {

    public function testWithNameInConstructor() {
        $pv = new PageView();
        $pv->add(new Marker('MyMarker'));
        $dataLayer = $pv->getDataLayer();
        $this->assertEquals('MyMarker', $dataLayer['marker'][0][0]);
    }

    public function testWithArrayInConstructor() {
        $pv = new PageView();
        $pv->add(new Marker(['name' => 'MyMarker']));
        $dataLayer = $pv->getDataLayer();
        $this->assertEquals('MyMarker', $dataLayer['marker'][0][0]);
    }

    public function testMultipleMarkersMustBeMergedInResult() {
        $pv = new PageView();
        $pv->add(new Marker('MyMarker1'));
        $pv->add(new Marker('MyMarker2'));
        $dataLayer = $pv->getDataLayer();
        $this->assertEquals('MyMarker1', $dataLayer['marker'][0][0]);
        $this->assertEquals('MyMarker2', $dataLayer['marker'][1][0]);
    }


}
