<?php

namespace Econda\Tracking;
use PHPUnit\Framework\TestCase;

class RegistrationTest extends TestCase {

    public function testWithUserIdInConstructor() {
        $pv = new PageView();
        $pv->add(new Registration('USER-ID', 15));
        $dataLayer = $pv->getDataLayer();
        $this->assertEquals('USER-ID', $dataLayer['register'][0]);
        $this->assertEquals(15, $dataLayer['register'][1]);
    }

    public function testWithNumbericUserIdInConstructor() {
        $pv = new PageView();
        $pv->add(new Registration(1234, 15));
        $dataLayer = $pv->getDataLayer();
        $this->assertEquals(1234, $dataLayer['register'][0]);
        $this->assertEquals(15, $dataLayer['register'][1]);
    }

    public function testWithArrayInConstructor() {
        $pv = new PageView();
        $pv->add(new Registration(['userId' => 'USER-ID']));
        $dataLayer = $pv->getDataLayer();
        $this->assertEquals('USER-ID', $dataLayer['register'][0]);
    }

    public function testWithDataSetAsProperty() {
        $reg = new Registration();
        $reg->userId = 'USER-ID';
        $reg->errorCode = 15;
        
        
        $pv = new PageView();
        $pv->add($reg);
        $dataLayer = $pv->getDataLayer();
        
        $this->assertEquals('USER-ID', $dataLayer['register'][0]);
        $this->assertEquals(15, $dataLayer['register'][1]);

    }

}
