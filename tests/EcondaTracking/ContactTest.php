<?php

namespace Econda\Tracking;

use PHPUnit\Framework\TestCase;

class ContactTest extends TestCase {

    public function testWithNameInConstructor() {
        $pv = new PageView();
        $pv->add(new Contact('MyFormName'));
        $dataLayer = $pv->getDataLayer();
        $this->assertEquals('MyFormName', $dataLayer['scontact']);
    }

    public function testWithArrayInConstructor() {
        $pv = new PageView();
        $pv->add(new Contact(['formName' => 'MyFormName']));
        $dataLayer = $pv->getDataLayer();
        $this->assertEquals('MyFormName', $dataLayer['scontact']);
    }

    public function testWithNameSetAsProperty() {
        $contact = new Contact();
        $contact->formName = 'MyFormName';
        
        $pv = new PageView();
        $pv->add($contact);
        $dataLayer = $pv->getDataLayer();
        
        $this->assertEquals('MyFormName', $dataLayer['scontact']);
    }

}
