<?php

namespace Econda\Tracking;
use PHPUnit\Framework\TestCase;

class TargetTest extends TestCase {

    public function testWithValuesInConstructor() {
        $pv = new PageView();
        $pv->add(new Target('TGroup', 'TName'));
        $dataLayer = $pv->getDataLayer();
        $this->assertEquals('TGroup', $dataLayer['Target'][0]);
        $this->assertEquals('TName', $dataLayer['Target'][1]);
        $this->assertEquals(0, $dataLayer['Target'][2]);
        $this->assertEquals('d', $dataLayer['Target'][3]);
    }

    public function testWithArrayInConstructor() {
        $pv = new PageView();
        $pv->add(new Target(['group' => 'TGroup', 'name' => 'TName', 'value' => 10.4, 'provision' => Target::PROVISION_ALL]));
        $dataLayer = $pv->getDataLayer();
        $this->assertEquals('TGroup', $dataLayer['Target'][0]);
        $this->assertEquals('TName', $dataLayer['Target'][1]);
        $this->assertEquals(10.4, $dataLayer['Target'][2]);
        $this->assertEquals('a', $dataLayer['Target'][3]);
    }

}
