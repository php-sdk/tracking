<?php
    ini_set('display_errors', true);

    require_once '../bootstrap.php';

    $pageView = new \Econda\Tracking\PageView([
       'siteId' => 'mySiteId',
       'contentLabel' => 'myContent/Label'
    ]);
?>
<html>
  <head>
    <!-- Simple Page View - nothing was initialized before -->
    <?php echo $pageView; ?>

    <!-- Simple Page View - but we have set a default value before, must still exist -->
    <script type="text/javascript">
      window.emos3 = {
          defaults: {
            myValue: 'must exist'
          }
      };
    </script>
    <?php echo $pageView; ?>
    
  </head>
</html>