php-sdk
===============

Helper classes to generate JavaScript code for econda tracking in PHP.
[![pipeline status](https://git.econda.de/php-sdk/tracking/badges/master/pipeline.svg)](https://git.econda.de/php-sdk/tracking/commits/master)
[![coverage report](https://git.econda.de/php-sdk/tracking/badges/master/coverage.svg)](https://git.econda.de/php-sdk/tracking/commits/master)

### Installing via Composer

The recommended way to install this library is through [Composer](http://getcomposer.org).


```bash
# Install Composer under Debian/Ubuntu
sudo apt-get install composer

# Install Composer
curl -sS https://getcomposer.org/installer | php

# Add Guzzle as a dependency
php composer.phar require econda/tracking:dev-master
```

After installing, you need to require Composer's autoloader:

```php
require 'vendor/autoload.php';
```

### Documentation
For further documentation, please visit our support portal https://support.econda.de


### Development
* Preconditions: Vagrant, Virtualbox, Cygwin (for vagrant ssh)
* Checkout project
* Run Cygwin
* cd project dir
* Start and provision vm: `vagrant up`
* Login to vm: `vagrant ssh`
* Run tests: `cd /vagrant/tests && ./run.sh`

Vm contains apache2 webserver with php support. View test pages (/test/manual/..) in browser listening on http://localhost:9888

### Deploy New Version
* Open project in gitlab: https://git.econda.de/php-sdk/tracking
* Add tag: v EMOS_MAJOR_VERSION . THIS_LIB_MINOR . THIS_LIB_BUGFIX, e.g. v3.0.1 (first bugfix version intended for emos 3 tracking)
* Packagist project should have been updated: https://packagist.org/packages/econda/tracking