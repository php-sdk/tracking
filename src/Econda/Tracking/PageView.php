<?php
namespace Econda\Tracking;

use Econda\Util\BaseObject;

/**
 * Contains all information to be tracked in current page view.
 * 
 * Preconditions:
 * 1. Set defaults:
 *     <script type="text/javascript">
window.emos3 = {
    defaults : {
        siteid : 'My Shop'   // set defaults if required
                             // defaults can be completely skipped if not used
    },
    stored : [],
    send : function(p){this.stored.push(p);}
};
</script>
 * 2. Include tracking library
 *     <script type="text/javascript" src="path/to/emos3.js"></script>
 *
 * @property string $langId Language id
 * @property string $countryId Country id
 * @property string $siteId Site id as configured in analytics account
 * @property string $contentLabel Path of page, separate path elements by slashes (/)
 */
class PageView extends BaseObject {
    
    protected $countryId;
    protected $langId;
    protected $siteId;
    protected $contentLabel;
    protected $pageId;
    protected $property;

    protected $data = array();
    
    /**
     * Add tracking data to page view
     * 
     * @param \Econda\Tracking\TrackingItemInterface $item
     * @return \Econda\Tracking\PageView
     */
    public function add(TrackingItemInterface $item) {
        
        $this->data[] = $item;
        return $this;
    }

    /**
     * set properties for custom dimensions
     *
     * @param $property
     */
    public function setProperty($property) {
        $this->property = $property;
    }
    
    /**
     * Returns data to be used for global properties. This data will be used as default for
     * additional requests on same page (via javascript)
     * @return array
     */
    public function getGlobalDataLayer() {
        $layer = array();

        // typensicher !
        if($this->countryId !== false) {
            $layer['countryid'] = trim($this->countryId);
        }
        // typensicher !
        if($this->langId !== false) {
            $layer['langid'] = trim($this->langId);
        }
        if(!empty($this->siteId)) {
            $layer['siteid'] = trim($this->siteId);
        }
        if(!empty($this->contentLabel)) {
            $layer['content'] = trim($this->contentLabel);
        }
        if(!empty($this->pageId)) {
            $layer['pageId'] = $this->pageId;
        } else {
            $layer['pageId'] = md5($this->contentLabel);
        }

        if(!empty($this->property)) {
            foreach($this->property as $key => $value){
                if(!is_bool($value)) {
                    $layer[$key] = $value;
                }
            }
        }

        return $layer;
    }
    
    /**
     * Returns data structure for tracking request
     * @return array
     */
    public function getRequestDataLayer() {
        $layer = array();

        foreach($this->data as $item) {

            $fragment = $item->getTrackingData();
            if(is_array($fragment)) {
                foreach($fragment as $key => $value) {
                    switch(true){
                        case (!empty($layer[$key]) && is_array($value)):
                            $layer[$key] = array_merge($layer[$key], $value);
                            break;
                        default:
                            $layer[$key] = $value;
                            break;
                    }
                }
            }
        }
        
        return $layer;
    }

    /**
     * Returns complete data layer which is a merged version of global and per request properties
     * @return array
     */
    public function getDataLayer() {

        return array_merge($this->getGlobalDataLayer(), $this->getRequestDataLayer());
    }
    
    /**
     * JSON encode value using correct encoding options
     * @param mixed $value
     * @return string
     */
    protected function jsonEncode($value) {
        return json_encode($value, JSON_HEX_AMP | JSON_HEX_QUOT | JSON_HEX_TAG);
    }
    
    /**
     * Returns HTML script node including all data
     */
    public function getAsJavaScriptNode() {
        $globalData = $this->getGlobalDataLayer();
        $requestData = $this->getRequestDataLayer();

        $html = array(
            '<script type="text/javascript">',
            '  if(typeof window.emos3 !== "object" || window.emos3 === null) { window.emos3 = {}; }',
            '  (function(emos) { ',
            '      (typeof emos.defaults === "object" && typeof emos.defaults !== null) || (emos.defaults = {});',
            '      (typeof emos.stored === "object" && typeof emos.stored.push === "function") || (emos.stored = []);',
            '      (typeof emos.send === "function") || (emos.send = function(p){this.stored.push(p)});',
            '      var pageDefaults = ' . $this->jsonEncode($globalData) . ';',
            '      for(var p in pageDefaults) { emos.defaults[p] = pageDefaults[p]; }',
            '      var requestData = ' . ($requestData ? $this->jsonEncode($requestData) : '{}') . ';',
            '      emos.send(requestData);',
            '  })(window.emos3);',
            '</script>',
        );
        return implode("\n", $html) . "\n";
    }
    
    public function __toString() {
        return $this->getAsJavaScriptNode();
    }
}
