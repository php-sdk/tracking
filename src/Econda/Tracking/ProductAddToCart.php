<?php
namespace Econda\Tracking;

use Econda\Util\BaseObject;

/**
 * Track product add to cart action
 *
 * @property Econda\Tracking\TransactionProduct $transactionProduct Product data.
 */
class ProductAddToCart extends BaseObject implements TrackingItemInterface {

    protected $product;

    /**
     * Constructor
     * @param $transactionProductOrPropertiesArray Transaction product object or properties array.
     */
    public function __construct($transactionProductOrPropertiesArray = null) {
        if (!empty($transactionProductOrPropertiesArray)) {
            if ($transactionProductOrPropertiesArray instanceof TransactionProduct) {
                $this->product = $transactionProductOrPropertiesArray;
            } else {
                parent::__construct($transactionProductOrPropertiesArray);
            }
        }
    }

    public function getTrackingData() {
        if ($this->product) {
            return array(
                'ec_Event' => array(
                    array_merge(array(
                        'type' => 'c_add',
                    ), $this->product->toArray())
                ),
            );
        } else {
            return null;
        }
    }

}
