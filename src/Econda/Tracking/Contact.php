<?php
namespace Econda\Tracking;

use Econda\Util\BaseObject;

/**
 * Description of Registration
 *
 * @property string $formName Human readable name of form
 */
class Contact extends BaseObject implements TrackingItemInterface {
    
    protected $formName;

    public function __construct($formNameOrPropertiesArray = null) {
        if($formNameOrPropertiesArray) {
            if(is_string($formNameOrPropertiesArray)) {
                $this->formName = $formNameOrPropertiesArray;
            } else {
                parent::__construct($formNameOrPropertiesArray);
            }
        }
    }
    
    public function getTrackingData() {
        return array('scontact' => $this->formName);
    }
    
}
