<?php
namespace Econda\Tracking;

/**
 * Items that can be added to a tracking request must implement this interface
 */
interface TrackingItemInterface {
    
    /**
     * Must return an json_encodable data structure. First item must be the key on page view object.
     * 
     * Example return value:
     * 
     *     array( "download" => "Label of Download" )
     */
    public function getTrackingData();
}
