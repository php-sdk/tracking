<?php
namespace Econda\Tracking;

use Econda\Util\BaseObject;

/**
 * Description of Registration
 *
 * @property string $userId Id or hash of user id
 * @property integer $errorCode Numeric error code or zero on success
 */
class Login extends BaseObject implements TrackingItemInterface {
    
    protected $userId;
    protected $errorCode = 0;

    public function __construct($userIdOrPropertiesArray = null, $errorCode = 0) {
        if(!is_null($userIdOrPropertiesArray)) {
            if(!is_array($userIdOrPropertiesArray)) {
                $this->setUserId($userIdOrPropertiesArray);
                $this->setErrorCode($errorCode);
            } else {
                parent::__construct($userIdOrPropertiesArray);
            }
        }
    }
    
    public function setUserId($userId) {
        $this->userId = (string) $userId;
    }

    public function setErrorCode($errorCode) {
        $this->errorCode = $errorCode * 1;
    }
    
    public function getTrackingData() {
        return array('login' => array($this->userId, $this->errorCode));
    }
    
}
