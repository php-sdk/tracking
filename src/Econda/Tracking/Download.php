<?php
namespace Econda\Tracking;

use Econda\Util\BaseObject;

/**
 * Description of Download
 *
 * @property string $name Name of download. It's ok but not required to use the file name.
 */
class Download extends BaseObject implements TrackingItemInterface {
    
    protected $name;
    
    /**
     * Constructor
     * @param type $nameOrPropertiesArray Name of download or an assoc array of property values.
     */
    public function __construct($nameOrPropertiesArray = null) {
        if(!empty($nameOrPropertiesArray)) {
            if(is_string($nameOrPropertiesArray)) {
                $this->name = trim($nameOrPropertiesArray);
            } else {
                parent::__construct($nameOrPropertiesArray);
            }
        }
    }
    
    public function getTrackingData() {
        return array(
            'download' => $this->name,
        );
    }
}
