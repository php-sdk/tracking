<?php

namespace Econda\Tracking;

use Econda\Util\BaseObject;

/**
 * Product detail view
 *
 * @property Econda\Tracking\Product $product Product data.
 */
class ProductDetailView extends BaseObject implements TrackingItemInterface {

    protected $product;

    /**
     * Constructor
     * @param type $productOrPropertiesArray Name of download or an assoc array of property values.
     */
    public function __construct($productOrPropertiesArray = null) {
        if (!empty($productOrPropertiesArray)) {
            if ($productOrPropertiesArray instanceof Product) {
                $this->product = $productOrPropertiesArray;
            } else {
                parent::__construct($productOrPropertiesArray);
            }
        }
    }

    public function getTrackingData() {
        if ($this->product) {
            return array(
                'ec_Event' => array(
                    array_merge(array(
                        'type' => 'view',
                        'count' => 1,
                            ), $this->product->toArray())
                ),
            );
        } else {
            return null;
        }
    }

}
