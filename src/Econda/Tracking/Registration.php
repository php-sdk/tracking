<?php
namespace Econda\Tracking;

use Econda\Util\BaseObject;

/**
 * Description of Registration
 *
 * @property string $userId Id or hash of user id
 * @property integer $errorCode Numeric error code or zero on success
 */
class Registration extends BaseObject implements TrackingItemInterface {
    
    protected $userId;
    protected $errorCode = 0;

    public function __construct($userIdOrPropertiesArray = null, $errorCode = 0) {
        if($userIdOrPropertiesArray) {
            if(is_string($userIdOrPropertiesArray) || is_numeric($userIdOrPropertiesArray)) {
                $this->userId = $userIdOrPropertiesArray;
                $this->errorCode = $errorCode;
            } elseif(is_array($userIdOrPropertiesArray)) {
                parent::__construct($userIdOrPropertiesArray);
            }
        }
    }
    
    public function setErrorCode($errorCode) {
        $this->errorCode = $errorCode * 1;
    }
    
    public function getTrackingData() {
        return array('register' => array($this->userId, $this->errorCode));
    }
    
}
