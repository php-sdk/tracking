<?php
namespace Econda\Tracking;

use Econda\Util\BaseObject;

/**
 * Data for a single product.
 * 
 * @property string $pid Article number of product. If product is a variant, article number of parent product.
 * @property string $name Product name without variant information.
 * @property string $group Product group
 * @property integer $price
 * @property string $var1 Value for variant 1
 * @property string $var2 Value for variant 2
 * @property string $var3 Value for variant 3
 */
class Product extends BaseObject {
    
    protected $pid;
    protected $sku;
    protected $name;
    protected $group;
    protected $price;
    protected $var1;
    protected $var2;
    protected $var3;
}
