<?php
namespace Econda\Tracking;

/**
 * Product item inside of transactions.
 *
 * @property integer $count Number of products in transaction.
 */
class TransactionProduct extends Product {
    protected $count = 1;
}
