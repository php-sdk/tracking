<?php
namespace Econda\Tracking;

use Econda\Util\BaseObject;

/**
 * Order process information.
 * 
 * Example
 *     $pageView->add(new Tracking\OrderProcess("1_CartOverview");
 *
 * @property string $name Name of current order process step. It's recommended to prefix the name by a step number (e.g. 1_Overview, 2_ContactData)
 */
class OrderProcess extends BaseObject implements TrackingItemInterface {
    
    protected $name;
    
    /**
     * Constructor
     * @param type $nameOrPropertiesArray Name of current step.
     */
    public function __construct($nameOrPropertiesArray = null) {
        if(!empty($nameOrPropertiesArray)) {
            if(is_string($nameOrPropertiesArray)) {
                $this->name = trim($nameOrPropertiesArray);
            } else {
                parent::__construct($nameOrPropertiesArray);
            }
        }
    }
    
    public function getTrackingData() {
        return array(
            'orderProcess' => $this->name,
        );
    }
}
