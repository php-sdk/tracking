<?php

namespace Econda\Tracking;

use Econda\Util\BaseObject;

/**
 * Tracking target. See https://support.econda.de/display/INDE/Ziele for details.
 *
 * @property string $group     Target group name
 * @property string $name      Target name
 * @property float  $value     Target value
 * @property string $provision Provisioning rule, allows one or multiple targets of same type per session.
 */
class Target extends BaseObject implements TrackingItemInterface {

    /**
     * Use settings as defined in analytics ui
     */
    const PROVISION_DEFAULT = 'd';

    /**
     * Allow one target (identified by name and group) per session. Additional targets will be ignored.
     */
    const PROVISION_ONE_PER_SESSION = 's';

    /**
     * Allow multiple targets of same type per session.
     */
    const PROVISION_ALL = 'a';

    protected $group;
    protected $name;
    protected $value;
    protected $provision = self::PROVISION_DEFAULT;

    public function __construct($targetGroupOrPropertiesArray = null, $targetName = null, $targetValue = null, $provision = null) {
        if ($targetGroupOrPropertiesArray) {
            if (is_string($targetGroupOrPropertiesArray)) {
                $this->group = $targetGroupOrPropertiesArray;
                $this->name = $targetName;
                if ($targetValue) {
                    $this->value = $targetValue;
                }
                if ($provision) {
                    $this->provision = $provision;
                }
            } else {
                parent::__construct($targetGroupOrPropertiesArray);
            }
        }
    }

    public function getTrackingData() {
        return array('Target' => array(
                $this->group,
                $this->name,
                $this->value,
                $this->provision
        ));
    }

}
