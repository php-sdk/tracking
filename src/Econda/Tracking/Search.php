<?php

namespace Econda\Tracking;

use Econda\Util\BaseObject;

/**
 * Used to track simple search interactions.
 * 
 *     $pageView->add(new Search('my search query', 5));
 * 
 * @property string $query User input, the search query
 * @property integer $numberOfHits Total number of results found for given query
 */
class Search extends BaseObject implements TrackingItemInterface {

    protected $query;
    protected $numberOfHits;

    public function __construct($queryOrPropertiesArray = null, $numberOfHits = null) {
        if (!is_null($queryOrPropertiesArray)) {
            if (is_string($queryOrPropertiesArray)) {
                $this->query = $queryOrPropertiesArray;
                if (!is_null($numberOfHits)) {
                    $this->numberOfHits = $numberOfHits;
                }
            } else {
                parent::__construct($queryOrPropertiesArray);
            }
        }
    }

    public function getTrackingData() {
        return array('search' => array(
                $this->query,
                $this->numberOfHits
        ));
    }

}
