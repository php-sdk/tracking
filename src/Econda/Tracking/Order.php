<?php

namespace Econda\Tracking;

use Econda\Util\BaseObject;

/**
 * Order data
 *
 * @property string $number Order number
 * @property string $customerId Customer id or hashed customer id
 * @property string $location Location, e.g. DE/7/76/Karlsruhe/76135
 * @property string $value  Order value
 * @property array  $products Array of transaction products in this order
 */
class Order extends BaseObject implements TrackingItemInterface {

    protected $number;
    protected $customerId;
    protected $location;
    protected $value;
    protected $products = array();

    /**
     * Constructor
     * @param type $propertiesArray Array with property values
     */
    public function __construct($propertiesArray = null) {
        parent::__construct($propertiesArray);
    }

    public function setProducts($orderedProducts) {
        if($orderedProducts && is_array($orderedProducts) === false) {
            $orderedProducts = array($orderedProducts);
        }
        if(is_array($orderedProducts)) {
            foreach($orderedProducts as $product) {
                if(($product instanceof TransactionProduct) === false) {
                    throw new \InvalidArgumentException("Products in an order must be an TransactionProduct.");
                }
            }
        }
        $this->products = $orderedProducts;
    }
    
    public function getTrackingData() {
        $productEvents = array();
        
        foreach($this->products as $transactionProduct) {
            $productEvents[] = array_merge(array('type' => 'buy'), $transactionProduct->toArray());
        }
        
        return array(
            'billing' => array($this->number, $this->customerId, $this->location, $this->value),
            'ec_Event' => $productEvents,
        );
    }

}
