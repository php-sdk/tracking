
apt-add-repository -y ppa:ansible/ansible
apt-get -q update

apt-get -y install git-core software-properties-common ansible mysql-client

# provision with ansible
cd /vagrant
ansible-playbook -i "localhost," -c local vagrant/ansible/setup-devhost.yml